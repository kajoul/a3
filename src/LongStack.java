import java.util.Arrays;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class LongStack {

    public static void main(String[] argum) {
        LongStack m = new LongStack();
        System.out.println(m);
        m.push(1);
        System.out.println(m);
        m.push(3);
        System.out.println(m);
        m.push(6);
        System.out.println(m);
        m.push(2);
        System.out.println(m);
        m.op("/");
        System.out.println(m);
        m.op("*");
        System.out.println(m);
        m.op("-");
        System.out.println(m);
        long tulemus = m.pop();
        System.out.println(m);
        System.out.println(tulemus);
        LongStack acopy = m;
        System.out.println(acopy.equals(m));
        System.out.println(m);
        System.out.println(acopy);
        try {
            acopy = (LongStack) m.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println(acopy.equals(m));
        System.out.println(m);
        System.out.println(acopy);
        m.push(6);
        System.out.println(acopy.equals(m));
        System.out.println(m);
        System.out.println(acopy);
        m.pop();
        System.out.println(acopy.equals(m));
        System.out.println(m);
        System.out.println(acopy);
        String prog = "2 3 + 4 * 10 /";
        if (argum.length > 0) {
            StringBuffer sb = new StringBuffer();
            for (String s : argum) {
                sb.append(s);
                sb.append(" ");
            }
            prog = sb.toString();
        }
        System.out.println(prog + "\n "
                + LongStack.interpret(prog));
    }

    private LinkedList<Long> linkedList;

    LongStack() {
        linkedList = new LinkedList<Long>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack tmp = new LongStack();
        tmp.linkedList = (LinkedList<Long>) linkedList.clone();
        return tmp;
    }

    public boolean stEmpty() {
        return linkedList.size() == 0;
    }

    public void push(long a) {
        linkedList.add(a);
    }

    public long pop() {
        if (stEmpty()) {
            throw new IndexOutOfBoundsException(" stack underflow");
        }
        return linkedList.removeLast();
    }

    public void op(String s) {
        long op2;
        long op1;

        try {
            op2 = pop();
            op1 = pop();
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException(" too few elements for " + s);
        }

        if (s.equals("+"))
            push(op1 + op2);
        else if (s.equals("-"))
            push(op1 - op2);
        else if (s.equals("*"))
            push(op1 * op2);
        else if (s.equals("/"))
            push(op1 / op2);
        else {
            throw new IllegalArgumentException("Invalid operation: " + s);
        }
    }

    public long tos() {
        if (stEmpty())
            throw new IndexOutOfBoundsException(" stack underflow");
        return linkedList.getLast();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LongStack) {
            if (((LongStack) o).linkedList.size() != linkedList.size())
                return false;
            for (int i = 0; i <= linkedList.size() - 1; i++) {
                if (!((LongStack) o).linkedList.get(i).equals(linkedList.get(i)))
                    return false;
            }
        } else {
            throw new ClassCastException("Not LongStack" + o);
        }
        return true;
    }

    @Override
    public String toString() {
        if (stEmpty())
            return "empty";
        StringBuffer b = new StringBuffer();
        for (int i = 0; i <= linkedList.size() - 1; i++)
            b.append(linkedList.get(i)).append(" ");
        return b.toString();
    }

    public static long interpret(String pol) {
        if (pol.isBlank())
            throw new IllegalArgumentException(pol + " is wrong expression");

        LongStack stack = new LongStack();
        String[] operators = {"+", "-", "/", "*"};

        StringTokenizer st = new StringTokenizer(pol);

        while (st.hasMoreTokens()) {
            String token = st.nextToken();

            if (Arrays.asList(operators).contains(token)) {
                if (stack.linkedList.size() >= 2) {
                    stack.op(token);
                } else {
                    throw new IndexOutOfBoundsException("too few elements for " + token + " operation in " + pol);
                }
                continue;
            }
            try {
                long longNumber = Long.parseLong(token);

                if (!st.hasMoreTokens() && !stack.stEmpty()) {
                    throw new RuntimeException("too many numbers in " + pol);
                }
                stack.push(longNumber);

            } catch (NumberFormatException e) {
                throw new IllegalArgumentException(token + " doesn't represent long or right operator in " + pol);
            }
        }

        return stack.pop();
    }
}

